import 'package:cf_qr_scanner/pages/b2bOrderitem.dart';
import 'package:cf_qr_scanner/pages/home_page.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class B2borderDetails extends StatefulWidget {

  final String code;
  B2borderDetails({
    this.code,
  });
  @override
  _B2borderDetailsState createState() => _B2borderDetailsState();
}

class _B2borderDetailsState extends State<B2borderDetails> {
  var selectedDelivery;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('B2B Order'),
      ),
      body: StreamBuilder(
        stream: Firestore.instance
            .collection('B2BorderProducts')
            .where('orderid', isEqualTo: widget.code)
            .snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Text('Loading');
          }
          return ListView.builder(
            itemCount: 1,
            itemBuilder: (context, index) {
              return _details(context, snapshot.data.documents[index]);
            },
          );
        },
      ),
     
    );
  }

  Future<void> _status(String id, String delivery) async {
    
    if (id == null) {
      return print('loading');
    } else {
      Firestore.instance
          .collection('track_order')
          .document(id)
          .updateData({
            'status':'Out of Delivery',
            'Delivery-Person':delivery,
          });
    }
    Fluttertoast.showToast(msg: 'Product Ready for Out of Delivery');
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => HomePage()));
  }

  
  Widget _details(BuildContext context, DocumentSnapshot documentSnapshot) {
    final GlobalKey<FormState> _formKeyValue = new GlobalKey<FormState>();
    
    return Container(
        child: Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: <Widget>[
              Text(
                'Customer Name  :',
                style: TextStyle(
                  fontSize: 22.0,
                  // fontWeight: FontWeight.bold
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: Text(
                  documentSnapshot['company']??'',
                  style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: <Widget>[
              Text(
                'Order ID  :',
                style: TextStyle(
                  fontSize: 22.0,
                  // fontWeight: FontWeight.bold
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: Text(
                  documentSnapshot['orderid']??'',
                  style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 3,
                child: Text(
                  'Address  :',
                  style: TextStyle(
                    fontSize: 22.0,
                    // fontWeight: FontWeight.bold
                  ),
                ),
              ),
              Expanded(
                flex: 7,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10.0),
                  child: Text(
                    documentSnapshot['address']??'',
                    style:
                        TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ],
          ),
        ),
        Divider(),
        Text('Product Details',
        style: TextStyle(
          fontSize: 22.0,
          color: Colors.red
        ),),
        Divider(),
        Container(
          child: B2bOrderItems(
            orderid: widget.code,
            documentid: documentSnapshot.documentID,
          ),
        ),
        Form(
          key: _formKeyValue,
          autovalidate: true,
          child: StreamBuilder<QuerySnapshot>(
            stream: Firestore.instance.collection('DeliveryExcecutive').snapshots(),
            builder: (context,snapshot){
              if(!snapshot.hasData){
                return Text('Loading');
              }
              else{
                List <DropdownMenuItem> deliveryExcecutiv = [];
                for(int i=0;i<snapshot.data.documents.length;i++){
                  DocumentSnapshot snap=snapshot.data.documents[i];
                  deliveryExcecutiv.add(
                    DropdownMenuItem(
                      child: Text(snap['Name']),
                      value: '${snap['uid']}',
                    ),
                  );
                }
                return Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10.0),
                  child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    
                    Container(
                      height: 60,
                      width: 200.0,
                      
                                          child: DropdownButton(
                        items: deliveryExcecutiv,
                        onChanged: (deliveryExcecutiv){
                          final snackBar = SnackBar(
                            content: Text('Selected Delivery Excecutive is  $deliveryExcecutiv'),
                          );
                          Scaffold.of(context).showSnackBar(snackBar);
                          setState(() {
                            selectedDelivery = deliveryExcecutiv;
                          });
                        },
                        value: selectedDelivery,
                        isExpanded: false,
                        hint: Text('Choose Delivery Boy',
                        style: TextStyle(
                          color: Color.fromRGBO(204, 51, 51, 1.0)
                        ),),
                      ),
                    )
                  ],
              ),
                );
              }
              
            },
          )
        ),
      
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: FlatButton(
          onPressed: () {
                  _status(documentSnapshot['orderid'],selectedDelivery);
                },
                child: Container(
            child: Center(child: Text('Out of Delivery',
            style: TextStyle(
              fontSize: 25.0,
              color: Colors.white
            ),)),
            color: Color.fromRGBO(204, 51, 51, 1.0),
            height: 70.0,
          ),
      ),
        ),
      ],
    ));
  }
}