import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class B2bOrderItems extends StatefulWidget {
  final String orderid,documentid;
  B2bOrderItems({
    this.orderid,
    this.documentid
  });
  @override
  _B2bOrderItemsState createState() => _B2bOrderItemsState();
}

class _B2bOrderItemsState extends State<B2bOrderItems> {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: Firestore.instance
          .collection('B2BorderProducts')
          .where('orderid', isEqualTo: widget.orderid)
          .snapshots(),
      builder: (context, snapshot) {
        final int cardlength = snapshot.data.documents.length;
        if (!snapshot.hasData) {
          return Text('Loading');
        }
        return ListView.builder(
          shrinkWrap: true,
          itemCount: cardlength,
          itemBuilder: (context, index) {
            return Column(
              children: <Widget>[
                _products(context, snapshot.data.documents[index]),
                Divider(),
              ],
            );
          },
        );
      },
    );
  }

  Widget _products(BuildContext context, DocumentSnapshot documentSnapshot) {
    return Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 7,
                child: Text(documentSnapshot['item']??'abc',
                style: TextStyle(fontWeight: FontWeight.bold),)),
              Expanded(
                flex: 3,
                child: Text(documentSnapshot['quantity']+' Kg'??''),
              )
            ],
          ),
        );
  }
  
}