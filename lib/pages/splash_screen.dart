import 'package:cf_qr_scanner/Components/sharedPreferences.dart';
import 'package:cf_qr_scanner/pages/home_page.dart';
import 'package:cf_qr_scanner/pages/pin_enter.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState(){
    super.initState();
    pinCheck();
  }

  Future<void> pinCheck() async{
    String pin = await SharedPref.getPin();
    if(pin==null){
      Navigator.pushReplacement(context, MaterialPageRoute(
        builder: (context)=>PinEnter()
      ));
    }
    else{
      Navigator.pushReplacement(context, MaterialPageRoute(
        builder: (context)=>HomePage()
      ));
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(child: Text('QR SCANNER',
      style: TextStyle(
        color: Colors.red,
        fontSize: 25.0
      ),)),
    );
  }
}