import 'package:cf_qr_scanner/Components/sharedPreferences.dart';
import 'package:cf_qr_scanner/pages/home_page.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class PinEnter extends StatefulWidget {
  @override
  _PinEnterState createState() => _PinEnterState();
}

class _PinEnterState extends State<PinEnter> {
  logIn(String pin) async {
    if (pin == '1234') {
      Navigator.pushReplacement(context, MaterialPageRoute(
        builder: (context)=>HomePage()
      ));
      Fluttertoast.showToast(
        msg: 'Login Successful'
      );
      SharedPref.setPin(pin);
    }else{
      Fluttertoast.showToast(
        msg: 'Invalid Pin'
      );
    }
  }

  String pin;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        child: Center(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 100.0),
            child: Container(
              child: PinCodeTextField(
                length: 4,
                animationType: AnimationType.fade,
                shape: PinCodeFieldShape.underline,
                animationDuration: Duration(milliseconds: 300),
                borderRadius: BorderRadius.circular(5),
                inactiveColor: Color.fromRGBO(204, 51, 51, 1.0),
                activeColor: Color.fromRGBO(51, 51, 102, 1.0),
                fieldHeight: 50.0,
                fieldWidth: 35.0,
                onChanged: (value) {
                  pin = value;
                },
              ),
            ),
          ),
        ),
      ),
      bottomNavigationBar: InkWell(
        child: Container(
          height: 50.0,
        color: Colors.red,
          child: Center(child: Text('Log In',
          style: TextStyle(
            color: Colors.white,
            fontSize: 20.0,
            fontWeight: FontWeight.bold
          ),)),
        ),
        onTap: () {
          logIn(pin);
          
        },
      ),
    );
  }
}
