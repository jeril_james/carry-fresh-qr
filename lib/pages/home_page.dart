import 'package:cf_qr_scanner/pages/b2borderdetails.dart';
import 'package:cf_qr_scanner/pages/orderDetails.dart';
import 'package:flutter/material.dart';
import 'package:qrscan/qrscan.dart' as scanner;

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String barcode;
  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      body: Container(
        child: Stack(
          children: <Widget>[
            
            Center(
              child: Container(
                width: MediaQuery.of(context).size.width * 0.8,
                margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.5),
                child: InkWell(
                  onTap: () {
                    _scan();
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(20.0)),
                      color: Color.fromRGBO(51, 51, 102, 1.0),
                    ),
                    height: 60.0,
                    child: Center(
                      child: Text('New Scan',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 20.0
                      ),),
                    ),
                  ),
                ),
              ),
            ),
            Center(
              child: Container(
                width: MediaQuery.of(context).size.width * 0.8,
                margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.7),
                child: InkWell(
                  onTap: () {
                    _scanB2B();
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(20.0)),
                      color: Color.fromRGBO(51, 51, 102, 1.0),
                    ),
                    height: 60.0,
                    child: Center(
                      child: Text('B2B Scan',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 20.0
                      ),),
                    ),
                  ),
                ),
              ),
            ),
            Center(
              child: Container(
                margin: EdgeInsets.only(top: MediaQuery.of(context).size.height *0.9),
                
                width: 70.0,
                child: Image.asset('images/carryfresh_phone.png'),
              ),
            )
          ],
        ),
      ),
    );
  }
  Future _scan() async{
    String barcode = await scanner.scan();
    setState(() {
      this.barcode = barcode;
    });
    Navigator.push(context, MaterialPageRoute(
      builder: (context)=>OrderDetails(
        code: barcode,
      )
    ));
    
  }

  Future _scanB2B() async{
    String barcode = await scanner.scan();
    setState(() {
      this.barcode = barcode;
    });
    Navigator.push(context, MaterialPageRoute(
      builder: (context)=>
        B2borderDetails(
          code: barcode,
        )
    ));
    // Fluttertoast.showToast(
    //   msg: 'Product Ready for Out of Delivery'
    // );
  }
}