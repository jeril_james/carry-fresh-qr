import 'package:cf_qr_scanner/Components/sharedPreferences.dart';
import 'package:cf_qr_scanner/pages/Errorpage.dart';
import 'package:cf_qr_scanner/pages/home_page.dart';
import 'package:cf_qr_scanner/pages/orderItems.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fluttertoast/fluttertoast.dart';

class OrderDetails extends StatefulWidget {
  final String code;
  OrderDetails({this.code});
  @override
  _OrderDetailsState createState() => _OrderDetailsState();
}

class _OrderDetailsState extends State<OrderDetails> {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: Firestore.instance
            .collection('Orders')
            .where('orderid', isEqualTo: widget.code)
            .where('status', isEqualTo: 'ordered')
            .snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Scaffold(
              appBar: AppBar(
                title: Text('data'),
              ),
              body: Container(
                child: Center(
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 20.0),
                        child: Image.asset('images/ErrorNormal.jpg'),
                      )
                    ],
                  ),
                ),
              ),
            );
          }
          return Scaffold(
            appBar: AppBar(
              title: Text('Normal Order'),
            ),
            body: ListView.builder(
              itemCount: 1,
              itemBuilder: (context, index) {
                return _details(context, snapshot.data.documents[index]);
              },
            ),
          );
        });
  }

  Future<void> _status() async {
    String id = await SharedPref.getid();
    if (id == null) {
      return print('loading');
    } else {
      Firestore.instance
          .collection('Orders')
          .document(id)
          .updateData({'status': 'Out of Delivery'});
    }
    Fluttertoast.showToast(msg: 'Product Ready for Out of Delivery');
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => HomePage()));
  }

  Widget _details(BuildContext context, DocumentSnapshot documentSnapshot) {
    SharedPref.setid(documentSnapshot.documentID);
    return Container(
        child: Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: <Widget>[
              Text(
                'Customer Name  :',
                style: TextStyle(
                  fontSize: 22.0,
                  // fontWeight: FontWeight.bold
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: Text(
                  documentSnapshot['customername'],
                  style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: <Widget>[
              Text(
                'Order ID  :',
                style: TextStyle(
                  fontSize: 22.0,
                  // fontWeight: FontWeight.bold
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: Text(
                  documentSnapshot['orderid'],
                  style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 3,
                child: Text(
                  'Address  :',
                  style: TextStyle(
                    fontSize: 22.0,
                    // fontWeight: FontWeight.bold
                  ),
                ),
              ),
              Expanded(
                flex: 7,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10.0),
                  child: Text(
                    documentSnapshot['address'],
                    style:
                        TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ],
          ),
        ),
        Divider(),
        Text(
          'Product Details',
          style: TextStyle(fontSize: 22.0, color: Colors.red),
        ),
        Divider(),
        Container(
          child: OrderItems(
            orderid: widget.code,
            documentid: documentSnapshot.documentID,
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: FlatButton(
                onPressed: () {
                  _status();
                },
                child: Container(
                  child: Center(
                      child: Text(
                    'Out of Delivery',
                    style: TextStyle(fontSize: 25.0, color: Colors.white),
                  )),
                  color: Color.fromRGBO(204, 51, 51, 1.0),
                  height: 70.0,
                ),
              ),
        ),
      ],
    ));
  }
}
