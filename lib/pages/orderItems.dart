import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class OrderItems extends StatefulWidget {
  final String orderid,documentid;
  OrderItems({
    this.orderid,
    this.documentid
  });
  @override
  _OrderItemsState createState() => _OrderItemsState();
}

class _OrderItemsState extends State<OrderItems> {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: Firestore.instance
          .collection('Orders')
          .document(widget.documentid)
          .collection('productdetails')
          .snapshots(),
      builder: (context, snapshot) {
        final int cardlength = snapshot.data.documents.length;
        if (!snapshot.hasData) {
          return Text('Loading');
        }
        return ListView.builder(
          shrinkWrap: true,
          itemCount: cardlength,
          itemBuilder: (context, index) {
            return ListView(
              shrinkWrap: true,
              children: <Widget>[
                _products(context, snapshot.data.documents[index]),
                Divider(),
              ],
            );
          },
        );
      },
    );
  }

  Widget _products(BuildContext context, DocumentSnapshot documentSnapshot) {
    return Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 7,
                child: Text(documentSnapshot['productname']??'',
                style: TextStyle(fontWeight: FontWeight.bold),)),
              Expanded(
                flex: 3,
                child: Text(documentSnapshot['quantity']+' Kg'??''),
              )
            ],
          ),
        );
  }
}
