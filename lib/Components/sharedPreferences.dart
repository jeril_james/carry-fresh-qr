import 'package:shared_preferences/shared_preferences.dart';


class SharedPref {
  static Future<void> setPin(String pin) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('pin', pin);
  }
  static Future<String> getPin() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('pin');
  }
  static Future<void> setid(String id) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('id', id);
  }
  static Future<String> getid() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('id');
  }
  static Future<void> setb2bid(String b2bid) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('b2bid', b2bid);
  }
  static Future<String> getb2bid() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('b2bid');
  }
}